import sys
import os
import csv
import numpy as np
import random

def get_Gi(i, P, K, alpha, N):
    sumG = 0
    for n in range (1,N):
        val = get_PathVal(n, i, P, K, alpha,N)
        sumG = sumG + val
    return sumG

def get_PathVal (n, i, P, K, alpha, N):

    val =0
    for t in range (0,N):
        #if P[i][t]!=0:
        if P[i,t]!=0:
            v2 = get_val(n-1, t, P, K, alpha, N)
           # val = val + v2*P[i][t]
            val = val + v2*P[i,t]
    return val


def get_val(n,t, P, K, alpha, N):
    if n==0:
        return K[t]
    val = 0
    for k in range(0, N):
        if P[t,k]!=0:
            V = get_val(n-1, k, P, K, alpha, N)
            #V = V * P[t][k]
            V = V * P[t,k]
            val = val + V
            val = val * alpha[t]
    return val

if __name__ == '__main__':

    P = np.matrix([[0,0.5,0.6,0,0,0],[0,0,0,0.4,0,0],[0,0,0,0,0.7,0],[0,0,0,0,0,0],[0,0,0,0,0,0.2],[0,0,0,0,0,0]])
    #print P
    #print P[0,1]
    K = [1,2,3,4,5,6]
    N = 6
    alpha = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6]
    sumG = get_Gi(0, P, K, alpha, N)
    print sumG


