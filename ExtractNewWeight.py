import sys
import os
import csv
import numpy
import random
from numpy import linalg as LA
#import division

def ReadNodeFile2(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 2:
          print 'format error : ', line
      #items = (terms[0], terms[1], terms[2], terms[3], terms[4],terms[5],terms[6])
      items = (terms[0], terms[1])
      outlist.append(items)
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist



def FilterList(rank,n):
    newrank = {}

    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']


    for x in range(0, len(Drugname11)):

        newrank[Drugname11[x]] = rank[Drugname11[x]]
    return newrank

def PrintInOrder (RankDict):

    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']

    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    for name in Namelist11:
        #print name
        #print name, ' ', RankDict[name]
        print RankDict[name]


if __name__ == '__main__':
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_09metric.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/node_11metric.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_09metric.csv'
    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/effectiveness.csv'
    infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/weightedpr.csv'
    infile3 = infile3.strip()
    effective = ReadNodeFile2(infile3)
    m = len(effective)
    wpr = []

    node = []
    '''
    authority = []
    hub = []
    pagerank = []
    ecce = []
    close = []
    between = []
    '''

    for gr in range(0, m):
        node.append(effective[gr][0])
        wpr.append(float(effective[gr][1]))
        '''
        node.append(effective[gr][0])
        authority.append(float(effective[gr][1]))
        hub.append(float(effective[gr][2]))
        pagerank.append(float(effective[gr][3]))
        ecce.append(float(effective[gr][4]))
        close.append(float(effective[gr][5]))
        between.append(float(effective[gr][6]))
        '''
    '''
    authority_dict = {}
    hub_dict = {}
    pagerank_dict = {}
    ecce_dict = {}
    close_dict = {}
    between_dict = {}
    '''
    weightpagerank_dict = {}

    for xl in range(0, m):
        '''
        authority_dict[node[xl]] = authority[xl]
        hub_dict[node[xl]] = hub[xl]
        pagerank_dict[node[xl]] = pagerank[xl]
        #review_dict[node[xl]] = review_pro[xl]
        ecce_dict[node[xl]] = ecce[xl]
        close_dict[node[xl]] = close[xl]
        between_dict[node[xl]] = between[xl]
        '''
        weightpagerank_dict[node[xl]] = wpr[xl]
    print weightpagerank_dict
    print "end"
    PrintInOrder(weightpagerank_dict)



