import sys
import os
import csv
import numpy
import random
'''
def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 9:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3], terms[4], terms[5], terms[6], terms[7], terms[8])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist

'''

def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 4:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist


def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist

def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)

def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else:
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])

    return weightdict

def WeightedInOutlink (A):
    m = len(A)
    #print m
    #print A
    wkin =[0]*m
    wkout = [0]*m
    switch =[0]*m
    combine = [0]*m
    #print wkin
    #print wkout
    for x in range(0,m):
        for y in range(0,m):
            #print A[x][y]
            wkin[x] = wkin[x]+A[x][y]
            wkout[x] = wkout[x]+A[y][x]
            switch[x] = wkin[x]-wkout[x]
            combine[x] = wkin[x] + wkout[x]
    return (switch, combine)

def FilterList(rank,n):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname)):

        newrank[Drugname[x]] = rank[Drugname[x]]
    return newrank

def PrintInOrder (RankDict):
    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']
    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    for name in Namelist:
        #print name
        #print name, ' ', RankDict[name]
        print RankDict[name]


def MultipleRankValue (rankdict, effedict,reviewdict):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname)):

        #newrank[Drugname[x]] = rankdict[Drugname[x]]*easedict[Drugname[x]]*safdict[Drugname[x]]*effedict[Drugname[x]]*reviewdict[Drugname[x]]
        newrank[Drugname09[x]] = rankdict[Drugname[x]]*effedict[Drugname[x]]*reviewdict[Drugname[x]]
    return newrank

def TotalDivision (list):
    total = 0
    listnew = [0]*m
    for t in range(0, m):
        total = total+list[t]
    for f in range(0, m):
        listnew[f] = list[f]/total
    return listnew

def ReadNodeFile2(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3], terms[4])
      outlist.append(items)
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist


if __name__ == '__main__':
    #infile ='/Users/YuanyuanGao/Desktop/drug-direct-graph (Nodes)-2.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_094.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    infile ='/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'

    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)
    A = numpy.zeros((n,n))
    #infile2 =  '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    infile2 =  '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/YuanyuanGao/Desktop/drug-direct-graph (Edges)-2.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/edges_093.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/edges_113.csv'
    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)
    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]=float(weightdict[m])

    #print notes
    #print A[342][234]

    (switch, combine) = WeightedInOutlink(A)

    switchrank = {}
    combinerank = {}
    for r in range(0,n):
        drugname = notes2[r]
        switchrank[drugname] = switch[r]
        combinerank[drugname] = combine[r]



    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/effectiveness.csv'
    infile3 = '/Users/u0810668/Dropbox/Drug graph projects/effectiveness.csv'
    #infile3 = '/Users/u0810668/Dropbox/Drug graph projects/effectiveness.csv'
    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/effective09.csv'
    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/effective11.csv'
    infile3 = infile3.strip()
    effective = ReadNodeFile2(infile3)
    m = len(effective)
    node = []
    effect = []
    ease = []
    satisfy = []
    review = []
    for gr in range(0, m):
        node.append(effective[gr][0])
        effect.append(float(effective[gr][1]))
        ease.append(float(effective[gr][2]))
        satisfy.append(float(effective[gr][3]))
        review.append(float(effective[gr][4]))

    effect_pro = TotalDivision(effect)
    ease_pro = TotalDivision(ease)
    satisfy_pro = TotalDivision(satisfy)
    review_pro = TotalDivision(review)



    effect_dict = {}
    ease_dict = {}
    satisfy_dict = {}
    review_dict = {}
    for xl in range(0, m):
        effect_dict[node[xl]] = effect_pro[xl]
        ease_dict[node[xl]] = ease_pro[xl]
        satisfy_dict[node[xl]] = satisfy_pro[xl]
        review_dict[node[xl]] = review_pro[xl]

    #print rank.keys()
    #print rank.values()



    newrankswitch = FilterList(switchrank, n)
    newrankcombine = FilterList(combinerank, n)
    print 'switch '
    PrintInOrder(newrankswitch)
    #PrintInOrder(newrankcombine)

    adjustedswitch = {}
    adjustedcombine = {}

    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    '''
    for name in Drugname09:

        adjustedswitch[name] = newrankswitch[name]*effect_dict[name]*review_dict[name]
        adjustedcombine[name] = newrankcombine[name]*effect_dict[name]*review_dict[name]
    '''
    #print effect_dict['zyprexa']
    #print newrankswitch['zyprexa']
    #print effect_dict['zyprexa']*newrankswitch['zyprexa']

    #PrintInOrder(adjustedswitch)
    #PrintInOrder(adjustedcombine)



