#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import numpy
import random
from numpy import linalg as LA

def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 2:
          print 'format error : ', line
      items = (terms[0], terms[1])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist

def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist

def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)

def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else:
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])

    return weightdict

def WeightedInOutlink (A):
    m = len(A)
    #print m
    #print A
    wkin =[0]*m
    wkout = [0]*m
    #print wkin
    #print wkout
    for x in range(0,m):
        for y in range(0,m):
            #print A[x][y]
            wkin[x] = wkin[x]+A[x][y]
            wkout[x] = wkout[x]+A[y][x]
    return (wkin, wkout)
    #return A

def WPageRank (A,wkin,wkout,dem_in,dem_out):
    m = len(A)
    d = 0.85
    pr = [0]*m
    sump = 0
    for t in range (0,m):
            pr[t] = random.uniform(0,1)
            #print pr[t]
    for i in range(0,100):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[x][y]!=0):

                    if (dem_in[y]!=0 and dem_out[y]!=0):
                        sumlist[x] = sumlist[x]+pr[y]*(wkin[x]/dem_in[y])*(wkout[x]/dem_out[y])
                    elif (dem_in[y]==0 and dem_out[y]!=0):
                        sumlist[x] = sumlist[x]+pr[y]*((wkin[x]+1)/(dem_in[y]+1))*(wkout[x]/dem_out[y])
                    elif (dem_in[y]!=0 and dem_out[y]==0):
                        sumlist[x] = sumlist[x]+pr[y]*(wkin[x]/dem_in[y])*((wkout[x]+1)/(dem_out[y]+1))
                    elif (dem_in[y]==0 and dem_out[y]==0):
                        sumlist[x] = sumlist[x]+pr[y]*((wkin[x]+1)/(dem_in[y]+1))*((wkout[x]+1)/(dem_out[y]+1))
                    '''
                    if (dem_in[y]!=0 and dem_out[y]!=0):
                        sumlist[x] = sumlist[x]+pr[y]*(wkin[x]/dem_in[y])*(wkout[x]/dem_out[y])
                    elif (dem_in[y]==0 and dem_out[y]!=0):
                        sumlist[x] = sumlist[x]+pr[y]*(wkout[x]/dem_out[y])
                    elif (dem_in[y]!=0 and dem_out[y]==0):
                        sumlist[x] = sumlist[x]+pr[y]*(wkin[x]/dem_in[y])
                    elif (dem_in[y]==0 and dem_out[y]==0):
                        sumlist[x] = sumlist[x]
                    '''
                else:
                    sumlist[x] = sumlist[x]
            pr[x] = (1-d)+d*sumlist[x]
            sump = sump + pr[x]
    print sump
    return pr

def InOutEdge(A):
    m = len(A)
    kin = [0]*m
    kout = [0]*m

    for x in range(0,m):
        for y in range(0,m):
            if (A[x][y]!=0):
                kin[x] = kin[x]+1
            else:
                kin[x] = kin[x]
            if (A[y][x]!=0):
                kout[x] = kout[x]+1
            else:
                kout[x] = kout[x]
    return (kin,kout)

def PageRank (A,kout):
     d = 0.85
     m = len(A)
     pr2 = [0]*m
     #print A
     for t in range (0,m):
            pr2[t] = random.uniform(0,1)
            #print pr2[t]
     #print 'kout', kout
     for i in range(0,100):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[x][y]!=0):
                    sumlist[x] = sumlist[x]+pr2[y]/kout[y]
                else:
                    sumlist[x] = sumlist[x]
            pr2[x] = (1-d)/m+d*sumlist[x]
     return pr2

def WeightDem (A,wkin,wkout):
    m = len(A)
    dem_in = [0]*m
    dem_out = [0]*m
    for x in range(0,m):
        for y in range(0,m):
            if (A[x][y]!=0 or A[y][x]!=0):
                dem_in[x] = dem_in[x]+wkin[y]
                dem_out[x] = dem_out[x]+wkout[y]

    return (dem_in, dem_out)

def RankID (pro2):
    rank_pr = {}
    for rx in range(0,n):
        drugnamex = notes2[rx]
        rank_pr[drugnamex] = pro2[rx]
    #print rank_pr.keys()
    #print rank_pr.values()
    return rank_pr

def FilterList(rank,n):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname11)):

        newrank[Drugname11[x]] = rank[Drugname11[x]]
    return newrank


def PrintInOrder (RankDict):
    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']
    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    for name in Namelist11:
        #print name
        #print name, ' ', RankDict[name]
        print RankDict[name]

if __name__ == '__main__':
    #infile ='/Users/YuanyuanGao/Desktop/drug-direct-graph (Nodes)-2.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/new edges/drug-direct-graph (Nodes)-2.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_094.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-09/node_094.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'


    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_094.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    infile = '/Users/YuanyuanGao/Dropbox/WeightedNB/WeightedprResults/Ynodemap.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-09/node_094.csv'

    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)

    A = numpy.zeros((n,n))
    #infile2 = '/Users/YuanyuanGao/Desktop/drug-direct-graph (Edges)-2.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/new edges/drug-direct-graph (Edges)-2.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/edges_093.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-09/edges_093.csv'
    infile2 = '/Users/YuanyuanGao/Dropbox/WeightedNB/WeightedprResults/Yedgemap.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'

    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/edges_093.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-09/edges_093.csv'

    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)

    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]=float(weightdict[m])
    B = numpy.array([[0,2,0], [5,0,4], [0,6,0]])
    (wkin, wkout) = WeightedInOutlink(A)

    #print wkin
    #print wkout

    (dem_in, dem_out) = WeightDem(A,wkin,wkout)
    #print dem_in
    #print dem_out


    pro = WPageRank(A,wkin,wkout,dem_in,dem_out)
    #print pro

    (kin, kout) = InOutEdge(A)
    #print 'kin', kin
    #print 'kout', kout

    pro2 = PageRank(A,kout)
    #print pro2


    rank_pr = RankID(pro)
    for x in rank_pr:
        print x, ", ", rank_pr[x]
    print rank_pr.keys()
    '''
    newrank = FilterList(rank_pr, n)
    print newrank.keys()
    print newrank.values()

    PrintInOrder(newrank)
    '''


