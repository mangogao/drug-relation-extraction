import sys
import os
import csv
import numpy
import random
from numpy import linalg as LA
#import division


def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 9:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3], terms[4], terms[5], terms[6], terms[7], terms[8])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist
  
def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist
  
def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)
  
def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else: 
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])
            
    return weightdict
    
def WeightedInOutlink (A):
    m = len(A)
    #print m
    #print A
    wkin =[0]*m
    wkout = [0]*m
    #print wkin
    #print wkout
    for x in range(0,m):
        for y in range(0,m):
            #print A[x][y]
            wkin[x] = wkin[x]+A[x][y]
            wkout[x] = wkout[x]+A[y][x]
    return (wkin, wkout)

def InOutEdge(A):
    m = len(A)
    kin = [0]*m
    kout = [0]*m
    
    for x in range(0,m):
        for y in range(0,m):
            if (A[x][y]!=0 and x!=y):
                kin[x] = kin[x]+1
            else:
                kin[x] = kin[x]
            if (A[y][x]!=0 and y!=x):
                kout[x] = kout[x]+1
            else:
                kout[x] = kout[x]
    return (kin,kout)
    
    
def WeightDem (A,wkin,wkout):
    m = len(A)
    dem_in = [0]*m
    dem_out = [0]*m
    '''
    for c in range(0,m):
        dem_in[c] = wkin[c]
        dem_out[c] = wkout[c]
    '''
    for x in range(0,m):
        for y in range(0,m):
            if (((x!=y) and (A[x][y]!=0)) or ((x!=y) and (A[y][x]!=0))):
                dem_in[x] = dem_in[x]+wkin[y]
                dem_out[x] = dem_out[x]+wkout[y]
                
    return (dem_in, dem_out)
    
def WinWeightedPR (A, wkin, kin, W, dem_in, alpha, n):
    Wnew = [0]*n
    e = 1
    for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[i][j]!=0)):
                    #Wnew[i] = Wnew[i]+(1+alpha[j]*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #print float(wkin[i]+e)
                    #print float(dem_in[j]+2*e)
                    #print float(wkin[i]+e)/float(dem_in[j]+2*e)
                    Wnew[i] = Wnew[i]+(alpha[j]*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
            Wnew[i] = kin[i] + Wnew[i]

        
    return Wnew
    

def LoseWeightedPR (A, wkout, kout, L, dem_out, alpha, n):
    Lnew = [0]*n
    e = 1

    for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[j][i]!=0)):
                    #Lnew[i] = Lnew[i]+(1+alpha[j]*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                    Lnew[i] = Lnew[i]+(alpha[j]*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
            Lnew[i] =kout[i]+Lnew[i]

    return Lnew

    
def TransProb(A,e,n,k):
    T = numpy.zeros((n,n))
    P = numpy.zeros((n,n))
    Q = numpy.zeros((n,n))
    a = [0]*n
    for y in range(0,n):
        Psum =0
        for x in range(0,n):
            if (x!=y and A[x][y]!=0):
                T[x][y] = float(A[x][y]+e)/float(A[x][y]+A[y][x]+2*e)
                P[x][y] = float(T[x][y]/k[x])
                Q[x][y] = float(T[x][y]/k[x])
                Psum += P[x][y]
        if (Psum!=0):
            a[y] = 1/Psum
        else:
            a[y] = 1
    
    for p in range(0,n):
        for q in range(0,n):
            P[q][p] = P[q][p]*a[p]
    return (T, P, Q)
    
def Rank (notes,Score):
    Rank = {}
    for r2 in range(0,n):
        drugnamex = notes[r2]
        Rank[drugnamex] = Score[r2]
    return Rank
    
def TotalDegree (kin, kout, n): 
    k = [0]*n
    for c in range(0,len(kin)):
        k[c]=kin[c]+kout[c]
        
    return k

def ComputeW(A,alpha,kin,n):
    W = [0]*n
    for w in range(0,n):
        W[w] = random.uniform(0,1)
        #print W[w]
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[i][j]!=0):
                    #print A[i][j]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*A[i][j]
                    #sumlist[i] = sumlist[i]+(1+alpha*W[j])*A[i][j]
                else:
                    sumlist[i] = sumlist[i]
            #W[i] = sumlist[i]+kin[i]
            W[i] = sumlist[i]
    return W
    

def ComputeL(A,alpha,kout,n):
    L = [0]*n
    #for l in range(0,n):
        #L[l] = random.uniform(0,1)
        #print L[l]
    for y in range(0,50):
        sumlist =[0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[j][i]!=0):
                    #print j,' ', i, ' ', A[j][i]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*A[j][i]
                    #sumlist[i] = sumlist[i]+(1+alpha*L[j])*A[j][i]
                else:
                    sumlist[i] = sumlist[i]
            #L[i] = sumlist[i]+kout[i]
            L[i] = sumlist[i]
    return L

def Score (W, L, n):
    score = [0]*n
    for s in range (0, n):
        score[s] = W[s]-L[s]
    return score

def PageRank_W(A, kout):
     d = 0.85
     m = len(A)
     pr2 = [0]*m
     #print A
     for t in range(0,m):
            pr2[t] = random.uniform(0,1)
            #print pr2[t]
     #print 'kout', kout
     for i in range(0,50):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[x][y]!=0):
                    sumlist[x] = sumlist[x]+pr2[y]/kout[y]
                else:
                    sumlist[x] = sumlist[x]
            pr2[x] = (1-d)/m+d*sumlist[x]
     return pr2

def PageRank_L(A, kin):
     d = 0.85
     m = len(A)
     pr2 = [0]*m
     #print A
     for t in range(0,m):
            pr2[t] = random.uniform(0,1)
            #print pr2[t]
     #print 'kout', kout
     for i in range(0,50):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[y][x]!=0):
                    sumlist[x] = sumlist[x]+pr2[y]/kin[y]
                else:
                    sumlist[x] = sumlist[x]
            pr2[x] = (1-d)/m+d*sumlist[x]
     return pr2

def AverageK (kin, kout, n):
    totaledge = 0
    for x in range(0,len(kin)):
        totaledge+=kin[x]+kout[x]
        #print kin[x]+kout[x]
    avek = float(totaledge/n)
    #print avek
    alpha = 2*avek/(pow(avek,2)-avek)
    return alpha
'''
def IndK (kin, kout, n):
    totalk = [0]*n
    alpha = [0]*n
    for x in range(0,n):
        totalk[x] = kin[x]+kout[x]
        alpha[x] = 2*totalk[x]/(pow(totalk[x], 2)-totalk[x])
    return alpha
'''
def FilterList(rank,n):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname)):

        newrank[Drugname[x]] = rank[Drugname[x]]
    return newrank

if __name__ == '__main__':
    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)
    eigen = []
    close = []
    between = []
    authority = []
    hub = []
    for g in range(0,n):
        eigen.append(float(resultlist[g][5]))
        close.append(float(resultlist[g][2]))
        between.append(float(resultlist[g][3]))
        authority.append(float(resultlist[g][7]))
        hub.append(float(resultlist[g][8]))
    #print notes
    #print eigen

    A = numpy.zeros((n,n))
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    infile2 = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    
    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)
    
    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]=float(weightdict[m])  
            
    #print A
    #print A[211][7]
    #print A[356][105]
    #print A[211][376]
   # print A[10][9]

    ########Aij = weightedPR ###############################################################
    #B = numpy.array([[0,2,0], [5,0,4], [0,6,0]]) 
    #print B  
    #C = numpy.array([[3,4,0],[6,7,0],[1,2,3]])
    kin,kout = InOutEdge(A)
    #print 'kin ', kin, 'kout ', kout
    
    (wkin, wkout) = WeightedInOutlink(A)
    #print 'wkin:', wkin, ' ', 'wkout:', wkout
    
    (dem_in, dem_out) = WeightDem(A, wkin, wkout)
    #print 'dem_in ', dem_in, 'dem_out ', dem_out
 
    #eigen =[1,1,1]

    alpha = AverageK(kin, kout, n)
    #alpha = IndK(kin, kout, n)
    print alpha
    #W_pr = WinWeightedPR(A, wkin, dem_in, eigen, n)
    #W_pr = WinWeightedPR(A, wkin, dem_in, alpha, n)
    #print W
    #L_pr = LoseWeightedPR(A, wkout, dem_out, eigen, n)
    #L_pr = LoseWeightedPR(A, wkout, dem_out, alpha, n)
    #print L
    
    #score_pr = Score(W_pr, L_pr, n)
    '''

    W_pr = [0]*n
    L_pr = [0]*n
    for t in range(0, n):
        W_pr[t] = random.uniform(0, 1)
        L_pr[t] = random.uniform(0, 1)
    for z in range(0, 100):
        W_pr = WinWeightedPR(A, wkin, kin, W_pr, dem_in, eigen, n)
        L_pr = LoseWeightedPR(A, wkout, kout, L_pr, dem_out, eigen, n)

    score_pr = Score(W_pr, L_pr, n)


    Rank1 = Rank(notes2, score_pr)
    print Rank1.keys()
    print Rank1.values()

    NewRank = FilterList(Rank1,n)
    print NewRank.keys()
    print NewRank.values()
    '''

    #######Aij = Transition Probability##################################################

    Totalk = TotalDegree(kin, kout, n)
    #print Totalk
    e = 1
    T, P, Q = TransProb(A, e, n, Totalk)
    #print T
    #print P
    #print Q
    #eigen2 = [0.5, 0.5, 0.5]

    W_tp = ComputeW(P, eigen, kin, n)
    #print W_tp
    L_tp = ComputeL(P, eigen, kout, n)
    #print L_tp
    score_tp = Score(W_tp, L_tp, n)
    #print score_tp
    rank_tp = Rank(notes2,score_tp)
    print rank_tp.keys()
    print rank_tp.values()
    NewRank = FilterList(rank_tp,n)
    print NewRank.keys()
    print NewRank.values()

    #### Network-based PR ######################################################
    '''
    pr_w = PageRank_W(A, kout)
    #pr_l = PageRank_L(A, kin)
    #pr_score = Score(pr_w, pr_l, n)
    #pr_rank = Rank(notes2, pr_score)
    pr_rank2 = Rank(notes2, pr_w)
    print pr_rank2.keys()
    print pr_rank2.values()
    '''