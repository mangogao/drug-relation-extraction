#!/usr/bin/python
#---coding:utf8----
import sys
import os
import csv
import numpy
import random
from numpy import linalg as LA
#import scipy.sparse
#from scipy import linalg as linalg
#from scipy.linalg import eig as eig

def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
     # if len(terms) != 2:
      if len(terms) != 9:
          print 'format error : ', line
      #items = (terms[0], terms[1])
      items = (terms[0], terms[1], terms[2], terms[3], terms[4], terms[5], terms[6], terms[7], terms[8])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist
  
def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist
  
def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)


def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else: 
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])
            
    return weightdict
    
def InOutEdge(A):
    kin = [0]*n
    kout = [0]*n
    
    for x in range(0,n):
        for y in range(0,n):
            if (A[x][y]!=0):
                kin[x] = kin[x]+1
            else:
                kin[x] = kin[x]
            if (A[y][x]!=0):
                kout[x] = kout[x]+1
            else:
                kout[x] = kout[x]
    return (kin,kout)
    
            
  
def ComputeW(A,kin, L, alpha,n):
    Wnew = [0]*n

    for i in range(0,n):
            for j in range(0,n):
                if(i!=j):
                    #Wnew[i] = alpha[j]*W[j]*A[i][j]+Wnew[i]
                    Wnew[i] = Wnew[i]+(1+alpha*W[j])*A[i][j]
            #Wnew[i] = kin[i]+Wnew[i]

    return Wnew
    
def ComputeL(A,kout, L,alpha,n):
    Lnew = [0]*n
    for i in range(0,n):
            for j in range(0,n):
                if(i!=j):
                    #Lnew[i] = alpha[j]*L[j]*A[j][i]+Lnew[i]
                    Lnew[i] = Lnew[i]+(1+alpha*L[j])*A[j][i]
            #Lnew[i] = kout[i]+Lnew[i]
    return Lnew

    
def TransProb(A,e,n,k):
    T = numpy.zeros((n,n))
    P = numpy.zeros((n,n))
    a = [0]*n
    for y in range(0,n):
        Psum =0
        for x in range(0,n):
            T[x][y] = (A[x][y]+e)/(A[x][y]+A[y][x]+2*e)
            P[x][y] = T[x][y]/k[x]
            Psum+= P[x][y]
        a[y] = 1/Psum
    
    for p in range(0,n):
        for q in range(0,n):
            P[q][p] = P[q][p]*a[p]
    return (T,P)

def FilterList(rank,n):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname)):

        newrank[Drugname[x]] = rank[Drugname[x]]
    return newrank


def near(a, b, rtol = 1e-5, atol = 1e-8):
    return numpy.abs(a-b)<(atol+rtol*numpy.abs(b))
    
def GetValueOne(evalue):
    min_i = 0
    min_v = -1
    print evalue.shape
    for i in range(evalue.shape[0]):
        if abs(evalue[i] - 1) < min_v:
            min_v = abs(evalue[i]-1)
            min_i = i 
    return min_i

if __name__ == '__main__':
    #infile ='/Users/YuanyuanGao/Desktop/drug-direct-graph (Nodes)-2.csv'
    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)
    #print notes
    eigen = []
    close = []
    between = []
    authority = []
    hub = []
    for g in range(0,n):
        eigen.append(float(resultlist[g][5]))
        close.append(float(resultlist[g][2]))
        between.append(float(resultlist[g][3]))
        authority.append(float(resultlist[g][7]))
        hub.append(float(resultlist[g][8]))
    #print notes
    #print notes2
    A = numpy.zeros((n,n))
    #infile2 = '/Users/YuanyuanGao/Desktop/drug-direct-graph (Edges)-2.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    infile2 = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)

    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]=float(weightdict[m])
            #print A[tid][sid]
    '''
    for z in range(0,n):
        print z, A[z][1]
        print z, A[1][z]
    '''

    #print A[46][1]
    
    kin,kout = InOutEdge(A)
    #print kin
    #print kout
    totaledge =0
    print n
    print len(kin)
    for x in range(0,len(kin)):
        totaledge+=kin[x]+kout[x]
        #print kin[x]+kout[x]
    avek = float(totaledge/n)
    #print avek
    
    alpha = 2*avek/(pow(avek,2)-avek)
    print alpha
    W = [0]*n
    L = [0]*n
    score = [0]*n
    for t in range (0,n):
        W[t] = random.uniform(0,1)
        L[t] = random.uniform(0,1)
        #print W[t]


    for z in range(0,100):
        W = ComputeW(A,kin,W,eigen,n)
        L = ComputeL(A,kout,L,eigen,n)
    for s in range(0,n):
            score[s]=W[s]-L[s]
    print W
    print L

    #B = numpy.array([[0,2,0], [5,0,4], [0,6,0]])
    #eigen2 = [0.5, 0.5, 0.5]
    #print eigen
    '''
    W = ComputeW(A,eigen,n)
    L = ComputeL(A,eigen,n)
    for s in range(0,n):
            score[s]=W[s]-L[s]
    '''
    #print W
    #print L
    #print score


    rank = {}
    for r in range(0,n):
        drugname = notes2[r]
        rank[drugname] = score[r]
    print rank.keys()
    print rank.values()

    newrank = FilterList(rank, n)
    print newrank.keys()
    print newrank.values()

    '''
    e=1
    k = [0]*n
    for c in range(0,len(kin)):
        k[c]=kin[c]+kout[c]
    #print k

    T,P =TransProb(A,e,n,k)

    Psum =0
    for z in range(0,n):
        #print z, T[z][1]
        print z, P[z][1]
        Psum = Psum + P[z][1]
        print ' '
    print Psum
        #print z, k[z]
        #print z, A[z][1]
        #print z, A[1][z]

        
        

    #evalue,evector= scipy.sparse.linalg.eig(P, k=1, sigma=1)
    
    evalue,evector = LA.eig(P)
    
    min_i = GetValueOne(evalue)
    min_eig_v = evalue[min_i]
    min_eig_vec = evector[:, min_i]
 
    #TransP = P.transpose()
    TransP = P.T
   
    Tevalue,Tevector = LA.eig(TransP)
    min_iT = GetValueOne(Tevalue)
    min_eig_v2 = Tevalue[min_iT]
    min_eig_vec2 = Tevector[:, min_iT]
    #print min_eig_vec2
    #print min_eig_v2
    
    scorex = min_eig_vec-min_eig_vec2
    print scorex

    rank2 = {}
    for r2 in range(0,n):
        drugnamex = notes2[r2]
        rank2[drugnamex] = scorex[r2]
    
    print rank2.keys()
    print rank2.values()

    newrank = FilterList(rank2, n)
    print newrank.keys()
    print newrank.values()
    '''
    
  
    
            
        
        
    
    
    
            
        
    
    
    
    
    
                

