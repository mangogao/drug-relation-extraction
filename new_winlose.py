import sys
import os
import csv
import numpy
import random
from numpy import linalg as LA
#import division


def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 9:
          print 'format error : ', line
      #items = (terms[0], terms[1], terms[2], terms[3])
      items = (terms[0], terms[1], terms[2], terms[3], terms[4], terms[5], terms[6], terms[7], terms[8])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist

def ReadNodeFile2(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3], terms[4])
      outlist.append(items)
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist
  
def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist
  
def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)
  
def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else: 
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])
            
    return weightdict
    
def WeightedInOutlink (A):
    m = len(A)
    #print m
    #print A
    wkin =[0]*m
    wkout = [0]*m
    #print wkin
    #print wkout
    for x in range(0,m):
        for y in range(0,m):
            #print A[x][y]
            wkin[x] = wkin[x]+A[x][y]
            wkout[x] = wkout[x]+A[y][x]
    return (wkin, wkout)

def InOutEdge(A):
    m = len(A)
    kin = [0]*m
    kout = [0]*m
    
    for x in range(0,m):
        for y in range(0,m):
            if (A[x][y]!=0 and x!=y):
                kin[x] = kin[x]+1
            else:
                kin[x] = kin[x]
            if (A[y][x]!=0 and y!=x):
                kout[x] = kout[x]+1
            else:
                kout[x] = kout[x]
    return (kin,kout)
    
    
def WeightDem (A,wkin,wkout):
    m = len(A)
    dem_in = [0]*m
    dem_out = [0]*m
    total_dem = [0]*m
    '''
    for c in range(0,m):
        dem_in[c] = wkin[c]
        dem_out[c] = wkout[c]
    '''
    for x in range(0,m):
        for y in range(0,m):
            if (((x!=y) and (A[x][y]!=0)) or ((x!=y) and (A[y][x]!=0))):
                dem_in[x] = dem_in[x]+wkin[y]
                dem_out[x] = dem_out[x]+wkout[y]
                total_dem[x] = dem_in[x]+dem_out[x]
                
    return (dem_in, dem_out, total_dem)
    
def WinWeightedPR (A,wkin,dem_in,alpha,n):
    W = [0]*n
    e = 1   
    for t in range (0,n):
            W[t] = random.uniform(0,1)
            #W[t] = random.randint(1,10)
            #print W[t]
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[i][j]!=0)):
                    sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #print float(wkin[i]+e)
                    #print float(dem_in[j]+2*e)
                    #print float(wkin[i]+e)/float(dem_in[j]+2*e)
                else:
                    sumlist[i] = sumlist[i]
            W[i] = sumlist[i]
        
    return W
    

def LoseWeightedPR (A,wkout,dem_out,alpha,n):
    L = [0]*n
    e = 1
    '''
    for t in range(0, n):
            L[t] = random.uniform(0, 1)
            #L[t] = random.randint(1,10)
            #print L[t]
    '''
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[j][i]!=0)):
                    sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                else:
                    sumlist[i] = sumlist[i]
            L[i] = sumlist[i]
        
    return L
    
def TransProb(A,e,n,k):
    T = numpy.zeros((n,n))
    P = numpy.zeros((n,n))
    Q = numpy.zeros((n,n))
    a = [0]*n
    for y in range(0,n):
        Psum =0
        for x in range(0,n):
            if (x!=y and A[x][y]!=0):
                T[x][y] = float(A[x][y]+e)/float(A[x][y]+A[y][x]+2*e)
                P[x][y] = float(T[x][y]/k[y])
                Q[x][y] = float(T[x][y]/k[y])
                Psum += P[x][y]
        if (Psum!=0):
            a[y] = 1/Psum
        else:
            a[y] = 1
    
    for p in range(0,n):
        for q in range(0,n):
            P[q][p] = P[q][p]*a[p]
    return (T, P, Q)
    
def Rank (notes,Score):
    Rank = {}
    for r2 in range(0,n):
        drugnamex = notes[r2]
        Rank[drugnamex] = Score[r2]
    return Rank
    
def TotalDegree (kin, kout, n): 
    k = [0]*n
    for c in range(0,len(kin)):
        k[c]=kin[c]+kout[c]
        
    return k

def ComputeW(A,alpha,n):
    W = [0]*n
    for w in range(0,n):
        W[w] = random.uniform(0,1)
        #print W[w]
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[i][j]!=0):
                    #print A[i][j]
                    #sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*A[i][j]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*A[i][j]
                else:
                    sumlist[i] = sumlist[i]
            W[i] = sumlist[i]
    return W
    

def ComputeL(A,alpha,n):
    L = [0]*n
    #for l in range(0,n):
        #L[l] = random.uniform(0,1)
        #print L[l]
    for y in range(0,50):
        sumlist =[0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[j][i]!=0):
                    #print j,' ', i, ' ', A[j][i]
                    #sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*A[j][i]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*A[j][i]
                else:
                    sumlist[i] = sumlist[i]
            L[i] = sumlist[i]

    return L

def Score (W, L, n):
    score = [0]*n
    for s in range (0, n):
        score[s] = W[s]-L[s]
    return score

def PageRank_W(A, kout):
     d = 0.85
     m = len(A)
     pr2 = [0]*m
     #print A
     for t in range(0,m):
            pr2[t] = random.uniform(0,1)
            #print pr2[t]
     #print 'kout', kout
     for i in range(0,50):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[x][y]!=0):
                    sumlist[x] = sumlist[x]+pr2[y]/kout[y]
                else:
                    sumlist[x] = sumlist[x]
            pr2[x] = (1-d)/m+d*sumlist[x]
     return pr2

def PageRank_L(A, kin):
     d = 0.85
     m = len(A)
     pr2 = [0]*m
     #print A
     for t in range(0,m):
            pr2[t] = random.uniform(0,1)
            #print pr2[t]
     #print 'kout', kout
     for i in range(0,50):
        sumlist = [0]*m
        for x in range(0,m):
            for y in range(0,m):
                if (A[y][x]!=0):
                    sumlist[x] = sumlist[x]+pr2[y]/kin[y]
                else:
                    sumlist[x] = sumlist[x]
            pr2[x] = (1-d)/m+d*sumlist[x]
     return pr2

def AverageK (kin, kout, n):
    totaledge = 0
    for x in range(0,len(kin)):
        totaledge+=kin[x]+kout[x]
        #print kin[x]+kout[x]
    avek = float(totaledge/n)
    #print avek
    alpha = 2*avek/(pow(avek,2)-avek)
    return alpha

def IndK (kin, kout, n):
    totalk = [0]*n
    alpha = [0]*n
    for x in range(0, n):
        totalk[x] = kin[x]+kout[x]
        alpha[x] = 2*totalk[x]/(pow(totalk[x], 2)-totalk[x])
    return alpha

def FilterList(rank,n):
    newrank = {}

    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']


    for x in range(0, len(Drugname11)):

        newrank[Drugname11[x]] = rank[Drugname11[x]]
    return newrank

def TotalDivision (list):
    total = 0
    listnew = [0]*m
    for t in range(0, m):
        total = total+list[t]
    for f in range(0, m):
        listnew[f] = list[f]/total
    return listnew

def MultipleRankValue (rankdict, easedict, safdict, effedict, reviewdict):
    newrank = {}
    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']


    for x in range(0, len(Drugname)):

        newrank[Drugname[x]] = rankdict[Drugname[x]]*easedict[Drugname[x]]*safdict[Drugname[x]]*effedict[Drugname[x]]*reviewdict[Drugname[x]]
        #newrank[Drugname[x]] = rankdict[Drugname[x]]*effedict[Drugname[x]]*reviewdict[Drugname[x]]
        #newrank[Drugname09[x]] = rankdict[Drugname09[x]]*reviewdict[Drugname09[x]]
    return newrank

def RelativeDivision (prolist):
    total = 5
    for rd in range(0, m):
        prolist[rd] = prolist[rd]/total
    return prolist

def PrintInOrder (RankDict):

    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']

    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    for name in Namelist11:
        #print name
        print name, ' ', RankDict[name]
        #print RankDict[name]

def NetworkDict(resultlist, networklist):
    l = len(resultlist)
    networkdict={}
    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']
    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in Namelist:
            networkdict[resultlist[i][0]] = networklist[i]


    return networkdict



def MinMax (k):
    kn = len(k)
    maxk = 0
    mink = 0
    for x in range(0,kn):
        if k[x]>maxk:
            maxk = k[x]
        else: maxk = maxk
        if k[x] <mink:
            mink = k[x]
        else: mink = mink
        if k[x] ==0:
            print x
    return (maxk, mink)

if __name__ == '__main__':
    infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_094.csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-09/node_094.csv'
    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)
    #print notes
    eigen = []
    close = []
    between = []
    #authority = []
    #hub = []
    for g in range(0,n):
        #eigen.append(float(resultlist[g][1]))
        eigen.append(float(resultlist[g][5]))
        close.append(float(resultlist[g][2]))
        between.append(float(resultlist[g][3]))
        #authority.append(float(resultlist[g][7]))
        #hub.append(float(resultlist[g][8]))

    betweendict = NetworkDict(resultlist, between)
    #print 'eigendict '
    #PrintInOrder(betweendict)
    #print 'end '

    closenew = [0]*n
    betweennew = [0.0]*n
    total_bw = 0
    for yl in range(0,n):
        total_bw = total_bw + between[yl]
  #  print total_bw

    for xl in range(0,n):
        if (close[xl]!=0):
            closenew[xl] = 1/close[xl]
            betweennew[xl] = between[xl]/total_bw
    #print closenew
    #print betweennew


    A = numpy.zeros((n,n))
    infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/edges_093.csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-09/edges_093.csv'
    
    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)
    #print weightdict
    
    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]= float(weightdict[m])
    #print A[70][195]


    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/effective09.csv'
    infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/effective11.csv'
    #infile3 = '/Users/u0810668/Dropbox/Drug graph projects/data-11/effective11.csv'
    #infile3 = '/Users/u0810668/Dropbox/Drug graph projects/data-09/effective09.csv'
    #infile3 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/effectiveness.csv'
    infile3 = infile3.strip()
    effective = ReadNodeFile2(infile3)
    m = len(effective)
    node = []
    effect = []
    ease = []
    satisfy = []
    review = []
    for gr in range(0, m):
        node.append(effective[gr][0])
        effect.append(float(effective[gr][1]))
        ease.append(float(effective[gr][2]))
        satisfy.append(float(effective[gr][3]))
        review.append(float(effective[gr][4]))

    effect_pro = TotalDivision(effect)
    ease_pro = TotalDivision(ease)
    satisfy_pro = TotalDivision(satisfy)
    review_pro = TotalDivision(review)
    effect_pro2 = RelativeDivision(effect)
    ease_pro2 = TotalDivision(ease)
    satisfy_pro2 = TotalDivision(satisfy)


    effect_dict = {}
    ease_dict = {}
    satisfy_dict = {}
    effect_dict2 = {}
    ease_dict2 = {}
    satisfy_dict2 = {}
    review_dict = {}
    for xl in range(0, m):
        effect_dict[node[xl]] = effect_pro[xl]
        ease_dict[node[xl]] = ease_pro[xl]
        satisfy_dict[node[xl]] = satisfy_pro[xl]
        #review_dict[node[xl]] = review_pro[xl]
        review_dict[node[xl]] = review[xl]
        effect_dict2[node[xl]] = effect_pro2[xl]
        ease_dict2[node[xl]] = ease_pro2[xl]
        satisfy_dict2[node[xl]] = satisfy_pro2[xl]
    #print ease_dict
    print review_dict
    print "end"
    PrintInOrder(review_dict)
    #print 'effect '
    #PrintInOrder(satisfy_dict)
    ########Aij = weightedPR ###############################################################

    #B = numpy.array([[0,2,0], [5,0,4], [0,6,0]]) 
    #print B  
    #C = numpy.array([[3,4,0],[6,7,0],[1,2,3]])
    kin,kout = InOutEdge(A)
    #print 'kin ', kin, 'kout ', kout
    
    (wkin, wkout) = WeightedInOutlink(A)
    #print 'wkin:', wkin, ' ', 'wkout:', wkout
    
    (dem_in, dem_out, total_dem) = WeightDem(A, wkin, wkout)
    #print 'dem_in ', dem_in, 'dem_out ', dem_out
    #print total_dem
    (max_k, min_k) = MinMax(total_dem)
    #print "MAX_K ", max_k
    #print "MIN_K ", min_k
    #eigen =[1,1,1]

    #alpha = AverageK(kin, kout, n)
    alpha = AverageK(wkin, wkout, n)
    #alpha = IndK(wkin, wkout, n)
    #print alpha

    W_pr = WinWeightedPR(A, wkin, dem_in, eigen, n)
   #W_pr = WinWeightedPR(A, wkin, dem_in, betweennew, n)
    #print W
    L_pr = LoseWeightedPR(A, wkout, dem_out, eigen, n)
    #L_pr = LoseWeightedPR(A, wkout, dem_out, betweennew, n)
    #print L

    score_pr = Score(W_pr, L_pr, n)

    Rank1 = Rank(notes2, score_pr)
    #print Rank1.keys()
    #print Rank1.values()

    NewRank = FilterList(Rank1,n)
    #print NewRank
    #print NewRank.keys()
    #print NewRank.values()
    #PrintInOrder(Rank1)

    #PrintInOrder(NewRank)


    #######Aij = Transition Probability##################################################

    Totalk = TotalDegree(kin, kout, n)
    #print Totalk
    e = 1
    T, P, Q = TransProb(A, e, n, Totalk)
    #print T
    #print P
    #print Q
    #eigen2 = [0.5, 0.5, 0.5]

    W_tp = ComputeW(P, eigen, n)
    #W_tp = ComputeW(P, betweennew, n)
    #print W_tp
    L_tp = ComputeL(P, eigen, n)
    #L_tp = ComputeL(P, betweennew, n)
    #print L_tp
    score_tp = Score(W_tp, L_tp, n)
    #print score_tp
    rank_tp = Rank(notes2, score_tp)

    #print rank_tp.keys()
    #print rank_tp.values()
    NewRank2 = FilterList(rank_tp, n)
    #PrintInOrder(NewRank2)
    #print NewRank2.keys()
    #print NewRank2.values()

    #MultiRank = MultipleRankValue(NewRank2, ease_dict, satisfy_dict, effect_dict, review_dict)
    #print MultiRank.keys()
    #print MultiRank.values()

    MultiRank2 = MultipleRankValue(NewRank2, ease_dict2, satisfy_dict2, effect_dict2, review_dict)
    #print MultiRank2.keys()
    #print MultiRank2.values()

    PrintInOrder(MultiRank2)
    #PrintInOrder(MultiRank)
    #PrintInOrder(satisfy_dict)


    #### Network-based PR ######################################################
    '''
    pr_w = PageRank_W(A, kout)
    #pr_l = PageRank_L(A, kin)
    #pr_score = Score(pr_w, pr_l, n)
    #pr_rank = Rank(notes2, pr_score)
    pr_rank2 = Rank(notes2, pr_w)
    print pr_rank2.keys()
    print pr_rank2.values()
    '''