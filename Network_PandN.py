import sys
import os
import csv
import numpy
import random
import numpy as np
from numpy import linalg as LA



def ReadNodeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 9:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2], terms[3])
      #items = (terms[0], terms[1], terms[2], terms[3], terms[4], terms[5], terms[6], terms[7], terms[8])
      outlist.append( items )
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist

def ReadNodeFile2(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 5:
          print 'format error : ', line
      #items = (terms[0], terms[1], terms[2], terms[3], terms[4])
      items = (terms[0], terms[1], terms[2], terms[3])
      outlist.append(items)
      #print ' name : ', terms[0], ' label : ', terms[1]
  In.close()
  return outlist

def ReadEdgeFile(infile):
  #print "y"
  if not os.path.exists(infile):
    print 'Error! file not exist! file path :', infile
    sys.exit()
  outlist = []
  In = open(infile, 'rU')
  for line in In.readlines():
    line = line.strip()
    if len(line) > 0:
      terms = line.split(',')
      if len(terms) != 3:
          print 'format error : ', line
      items = (terms[0], terms[1], terms[2])
      outlist.append( items )
      #print ' source : ', terms[0], ' target : ', terms[1], 'weight: ', terms[2]
  In.close()
  return outlist

def NoteDict(resultlist):
    l = len(resultlist)
    notes={}
    notes2 = {}
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in notes.keys():
            nid =nid
        else:
            notes[resultlist[i][0]]=nid
            notes2[nid]=resultlist[i][0]
            nid = nid +1
    return (notes,notes2)

def CombineDup(edgelist):
    weightdict = {}
    el = len(edgelist)
    for p in range(0,el):
        if (edgelist[p][0],edgelist[p][1]) in weightdict.keys():
            weightdict[edgelist[p][0],edgelist[p][1]]+=float(edgelist[p][2])
        else:
            weightdict[edgelist[p][0],edgelist[p][1]] =float(edgelist[p][2])

    return weightdict

def WeightedInOutlink (A):
    m = len(A)
    #print m
    #print A
    wkin =[0]*m
    wkout = [0]*m
    wtotal =[0]*m
    wdiff = [0]*m
    #print wkin
    #print wkout
    for x in range(0,m):
        for y in range(0,m):
            #print A[x][y]
            wkin[x] = wkin[x]+A[x][y]
            wkout[x] = wkout[x]+A[y][x]
            wtotal[x] = wtotal[x]+A[x][y]+A[y][x]
            wdiff[x] = wdiff[x]+A[x][y] - A[y][x]
    return (wkin, wkout, wtotal, wdiff)

def InOutEdge(A):
    m = len(A)
    kin = [0]*m
    kout = [0]*m

    for x in range(0,m):
        for y in range(0,m):
            if (A[x][y]!=0 and x!=y):
                kin[x] = kin[x]+1
            else:
                kin[x] = kin[x]
            if (A[y][x]!=0 and y!=x):
                kout[x] = kout[x]+1
            else:
                kout[x] = kout[x]
    return (kin,kout)

def WeightDem (A,wkin,wkout):
    m = len(A)
    dem_in = [0]*m
    dem_out = [0]*m
    total_dem = [0]*m
    '''
    for c in range(0,m):
        dem_in[c] = wkin[c]
        dem_out[c] = wkout[c]
    '''
    for x in range(0,m):
        for y in range(0,m):
            if (((x!=y) and (A[x][y]!=0)) or ((x!=y) and (A[y][x]!=0))):
                dem_in[x] = dem_in[x]+wkin[y]
                dem_out[x] = dem_out[x]+wkout[y]
                total_dem[x] = dem_in[x]+dem_out[x]

    return (dem_in, dem_out, total_dem)

def WinWeightedPR (A,wkin,dem_in,alpha,n):
    W = [0]*n
    e = 1
    for t in range (0,n):
            W[t] = random.uniform(0,1)
            #W[t] = random.randint(1,10)
            #print W[t]
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[i][j]!=0)):
                    sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*W[j])*(float(wkin[i]+e)/float(dem_in[j]+2*e))
                    #print float(wkin[i]+e)
                    #print float(dem_in[j]+2*e)
                    #print float(wkin[i]+e)/float(dem_in[j]+2*e)
                else:
                    sumlist[i] = sumlist[i]
            W[i] = sumlist[i]

    return W

def LoseWeightedPR (A,wkout,dem_out,alpha,n):
    L = [0]*n
    e = 1
    '''
    for t in range(0, n):
            L[t] = random.uniform(0, 1)
            #L[t] = random.randint(1,10)
            #print L[t]
    '''
    for x in range(0,50):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if((i!=j) and (A[j][i]!=0)):
                    sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                    #sumlist[i] = sumlist[i]+(1+alpha*L[j])*(float(wkout[i]+e)/float(dem_out[j]+2*e))
                else:
                    sumlist[i] = sumlist[i]
            L[i] = sumlist[i]

    return L

def TransProb(A,e,n,k):
    T = numpy.zeros((n,n))
    P = numpy.zeros((n,n))
    Q = numpy.zeros((n,n))
    X = numpy.zeros((n,n))
    F = numpy.zeros((n,n))
    a = [0]*n
    for y in range(0,n):
        Psum =0
        for x in range(0,n):
            if (x!=y and A[x][y]!=0):
                T[x][y] = float(A[x][y]+e)/float(A[x][y]+A[y][x]+2*e)
                X[x][y] = float(A[x][y]+e)/float(k[y]+2*e)
                P[x][y] = float(T[x][y]/k[y])
                Q[x][y] = float(T[x][y]/k[y])
                F[x][y] = A[x][y] + A[y][x]
                Psum += P[x][y]
        if (Psum!=0):
            a[y] = 1/Psum
        else:
            a[y] = 1

    for p in range(0,n):
        for q in range(0,n):
            P[q][p] = P[q][p]*a[p]
            X[q][p] = X[q][p]*a[p]
    return (T, P, Q, X, F)

def Rank (notes,Score):
    Rank = {}
    for r2 in range(0,n):
        drugnamex = notes[r2]
        Rank[drugnamex] = Score[r2]
    return Rank

def TotalDegree (kin, kout, n):
    k = [0]*n
    for c in range(0,len(kin)):
        k[c]=kin[c]+kout[c]

    return k

def Score (W, L, n):
    score = [0]*n
    for s in range (0, n):
        score[s] = W[s]-L[s]
    return score

def FilterList(rank,n):
    newrank = {}

    Drugname =['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    Drugname09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Drugname11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']

    for x in range(0, len(Drugname11)):

        newrank[Drugname11[x]] = rank[Drugname11[x]]
    return newrank

def PrintInOrder (RankDict):

    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']

    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    for name in Namelist11:
        #print name
        #print name, ' ', RankDict[name]
        print RankDict[name]

def NetworkDict(resultlist, networklist):
    l = len(resultlist)
    networkdict={}
    Namelist = ['xanax','zoloft','celexa','prozac','ativan','desyrel','lexapro','cymbalta','effexor',
                'valium','paxil','seroquel','risperdal','vyvanse','concerta','abilify','wellbutrin','buspar',
                'vistaril','amphetamine','zyprexa','methylphenidate','pristiq']
    Namelist09 = ['xanax','lexapro','ativan','zoloft','prozac','desyrel','cymbalta','seroquel','effexor',
                  'valium','amphetamine','risperdal','vistaril','bupropion','abilify','concerta','celexa',
                  'buspar','vyvanse','zyprexa','adderall','wellbutrin','geodon','strattera','pristiq']
    Namelist11 = ['xanax','celexa','zoloft','ativan','prozac','lexapro',
               'desyrel','cymbalta','valium','seroquel','paxil','effexor',
               'wellbutrin','risperdal','abilify','vyvanse','wellbutrin-SR','vistaril',
               'amphetamine','buspar','zyprexa','concerta','methylphenidate','pristiq']
    nid=0
    for i in range(0,l):
        if resultlist[i][0] in Namelist11:
            networkdict[resultlist[i][0]] = networklist[i]


    return networkdict



def MinMax (k):
    kn = len(k)
    maxk = 0
    mink = 0
    for x in range(0,kn):
        if k[x]>maxk:
            maxk = k[x]
        else: maxk = maxk
        if k[x] <mink:
            mink = k[x]
        else: mink = mink
        if k[x] ==0:
            print x
    return (maxk, mink)

def get_Gi(i, P, K, alpha, N):
    sumG = 0
    for n in range (1,N):
        val = get_PathVal(n, i, P, K, alpha,N)
        sumG = sumG + val
    return sumG

def get_PathVal (n, i, P, K, alpha, N):

    val =0
    for t in range (0,N):
        if P[i][t]!=0:
        #if P[i,t]!=0:
            v2 = get_val(i,n-1, t, P, K, alpha, N)
            val = val + v2*P[i][t]
            #val = val + v2*P[i,t]
    return val


def get_val(i,n,t, P, K, alpha, N):
    if n==0:
        #return K[i][t]
        #return K[t]
        return 1
    val = 0
    for k in range(0, 6):
        if P[t][k]!=0:
            V = get_val(i,n-1, k, P, K, alpha, N)
            V = V * P[t][k]
           #V = V * P[t,k]
            val = val + V
            val = val * alpha[t]
    return val

def get_Li(i, P, K, alpha, N):
    sumG = 0
    for n in range (1,N):
        val = get_PathVal_L(n, i, P, K, alpha,N)
        sumG = sumG + val
    return sumG

def get_PathVal_L (n, i, P, K, alpha, N):

    val =0
    for t in range (0,N):
        if P[t][i]!=0:
        #if P[i,t]!=0:
            v2 = get_val_L(i, n-1, t, P, K, alpha, N)
            val = val + v2*P[t][i]
            #val = val + v2*P[i,t]
    return val


def get_val_L(i, n,t, P, K, alpha, N):
    if n==0:
        return 1
    val = 0
    for k in range(0, 6):
        if P[k][t]!=0:
            V = get_val(i, n-1, k, P, K, alpha, N)
            V = V * P[k][t]
           #V = V * P[t,k]
            val = val + V
            val = val * alpha[t]
    return val


def ComputeL(A,alpha,n):
    L = [0]*n
    #for l in range(0,n):
        #L[l] = random.uniform(0,1)
        #print L[l]
    for y in range(0,10):
        sumlist =[0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[j][i]!=0):
                    #print j,' ', i, ' ', A[j][i]
                    #sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*A[j][i]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*L[j])*A[j][i]
                else:
                    sumlist[i] = sumlist[i]
            L[i] = sumlist[i]

    return L

def ComputeW(A,alpha,n):
    W = [0]*n
    for w in range(0,n):
        W[w] = random.uniform(0,1)
        #print W[w]
    for x in range(0,10):
        sumlist = [0]*n
        for i in range(0,n):
            for j in range(0,n):
                if(i!=j and A[i][j]!=0):
                    #print A[i][j]
                    #sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*A[i][j]
                    sumlist[i] = sumlist[i]+(1+alpha[j]*W[j])*A[i][j]
                else:
                    sumlist[i] = sumlist[i]
            W[i] = sumlist[i]
    return W

def Score (W, L, n):
    score = [0]*n
    for s in range (0, n):
        score[s] = W[s]-L[s]
    return score

if __name__ == '__main__':
    #infile ='/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(node).csv'
    #infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/node_094.csv'
    infile = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-11/nodes_113.csv'
    #infile = '/Users/u0810668/Dropbox/Drug graph projects/data-09/node_094.csv'
    infile = infile.strip()
    resultlist = ReadNodeFile(infile)
    notes,notes2 = NoteDict(resultlist)
    n=len(resultlist)
    #print notes
    eigen = []
    close = []
    between = []
    #authority = []
    #hub = []
    for g in range(0,n):
        eigen.append(float(resultlist[g][1]))
        #eigen.append(float(resultlist[g][5]))
        close.append(float(resultlist[g][2]))
        between.append(float(resultlist[g][3]))
        #authority.append(float(resultlist[g][7]))
        #hub.append(float(resultlist[g][8]))

    eigendict = NetworkDict(resultlist, eigen)
    print 'eigendict '
    PrintInOrder(eigendict)
    print 'end '

    closenew = [0]*n
    betweennew = [0.0]*n
    total_bw = 0
    for yl in range(0,n):
        total_bw = total_bw + between[yl]
  #  print total_bw

    for xl in range(0,n):
        if (close[xl]!=0):
            closenew[xl] = 1/close[xl]
            betweennew[xl] = between[xl]/total_bw

    A = numpy.zeros((n,n))
   # infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/direct-4(edge).csv'
    #infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-09/edges_093.csv'
    infile2 = '/Users/YuanyuanGao/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-11/edges_113.csv'
    #infile2 = '/Users/u0810668/Dropbox/Drug graph projects/data-09/edges_093.csv'

    edgelist = ReadEdgeFile(infile2)
    weightdict = CombineDup(edgelist)
    #print weightdict

    for m in weightdict:
        if(m[0] in notes.keys() and m[1] in notes.keys()):
            tid = notes[m[1]]
            sid = notes[m[0]]
            #print type(weightdict[m])
            #print m[0],' ',m[1]
            #print notes[m[0]],' ',notes[m[1]], ' ', weightdict[m]
            A[tid][sid]= float(weightdict[m])
    #print A[70][195]


    #######Aij = Transition Probability##################################################
    kin,kout = InOutEdge(A)
    #print 'kin ', kin, 'kout ', kout

    (wkin, wkout, wtotal, wdiff) = WeightedInOutlink(A)
    #print 'wkin:', wkin, ' ', 'wkout:', wkout

    (dem_in, dem_out, total_dem) = WeightDem(A, wkin, wkout)
    #print 'dem_in ', dem_in, 'dem_out ', dem_out
    #print total_dem
    (max_k, min_k) = MinMax(total_dem)
    Totalk = TotalDegree(kin, kout, n)
    #print Totalk
    e = 1
    #T, P, Q = TransProb(A, e, n, Totalk)
    T, P, Q, X, F = TransProb(A, e, n, kout)
    #print X
    #print P

    ########Get Value of Gi and Oi###############################################################

    G = [0]*n
    Lt = [0]*n
    '''
    for w in range(0,n):
        G[w] = get_Gi(w, A, wtotal, eigen, n)
        #Lt[w] = get_Li(w, A, wtotal, eigen, n)
    print G
    print "end G"
    '''
    #print Lt
    #print "end Lt"

    L = [0]* n
    L = ComputeL(P,eigen, n)
    #W = ComputeW(A,eigen, n)

    print L
    print "end L"
    #print wtotal
    #print "end totl"

    #score = Score(W,L,n)
    O = [0]*n
    L_n = [0]*n

    for o in range(0, n):
        #print L[o] * wkout[o]
        L_n[o] =  L[o] * wkout[o]
        #O[o]= G[o]-L[o] * wtotal[o]
        #O[o]= G[o]-Lt[o]
    #print O
    #print "end O"

    #Rank1 = Rank(notes2,wdiff)
    Rank1 = Rank(notes2, L_n)

    NewRank = FilterList(Rank1,n)

    PrintInOrder(NewRank)

